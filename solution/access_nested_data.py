for cit_status in metadata_cit_dict.keys(): # main keys "plus" and "minus"
    for sample_names in metadata_cit_dict[cit_status]:
        sample_values = metadata_cit_dict[cit_status][sample_names]
        genome_size = sample_values.split(",")[-1]
        if float(genome_size) < 4.6:
            print("Sample {} has {} cit status and its genome size is {}".format(
            sample_names, cit_status, genome_size))
