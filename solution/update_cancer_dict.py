in_fh = open("worldwide_cancer.tsv", "r")

cancer_rank_dict = {}

for cancer_data in in_fh:
    if not cancer_data.startswith("Rank"):
        rank = cancer_data.split('\t')[0]
        cancer = cancer_data.split('\t')[1]
        cancer_rank_dict[cancer] = rank

print(cancer_rank_dict)
