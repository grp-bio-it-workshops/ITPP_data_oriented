# 1. Read all the files

worldwide_cancer = open("worldwide_cancer.tsv", "r")
worldwide_male_cancer = open("worldwide_male_cancer.tsv", "r")
worldwide_female_cancer = open("worldwide_female_cancer.tsv", "r")

# 2. Create three empty lists to store cancer names from different files

all_cancer_list = []
male_cancer_list = []
female_cancer_list = []

# 3. Use `for` loop to go through the files and extract cancer names
# 4. Store cancer names in the lists created in the point 2.

for entry1 in worldwide_cancer:
    all_cancer_list.append(entry1.split('\t')[1])

for entry2 in worldwide_male_cancer:
    male_cancer_list.append(entry2.split('\t')[1])

for entry3 in worldwide_female_cancer:
    female_cancer_list.append(entry3.split('\t')[1])

## 5. Print all the lists.
# print('List of common cancers: {}'.format(all_cancer))
# print('List of common cancers in male: {}'.format(male_cancer))
# print('List of common cancers in female: {}'.format(female_cancer))

# 6. compare the lists to print common entries from all the lists.

for entry in male_cancer_list:
    if entry in female_cancer_list:
        print(entry)

# 7. Use intersection to print common entries from make and female cancer lists

print(set(male_cancer_list).intersection(female_cancer_list))
