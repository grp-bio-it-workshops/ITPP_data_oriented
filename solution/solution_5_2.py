def check_genome_size(value_dict):
    smaller_genome_size = []
    for key in value_dict:
        genome_size = value_dict[key][-1]
        if float(genome_size) < 4.6:
            smaller_genome_size.append(key)
    return smaller_genome_size

sample_metadata_dict = {}
in_fh = open("Ecoli_metadata.csv", "r")
for line in in_fh:
    if not line.startswith('sample'):
        sample_id = line.split(',')[0]
        sample_metadata_dict[sample_id] = line.strip().split(',') # we have used it in previous chapter

smaller_genome_size = check_genome_size(sample_metadata_dict)
print(smaller_genome_size)
