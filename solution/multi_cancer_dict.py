#1. Create an empty dict `cancer_cases_dict`.

cancer_cases_dict = {}

#2. Read "worldwide_cancer.tab" file

in_fh = open("worldwide_cancer.tab", "r")

#3. Store values for cases diagnosed (3rd column) for each cancer as a key (2nd column).

for entry in in_fh:
    cancer = entry.split('\t')[1]
    cases = entry.split('\t')[2]
    cancer_cases_dict[cancer] = cases

#4. Using the keys extract values from both the dictionaries and print them.

for key in cancer_rank_dict:
    print("Cancer {} has been ranked {} worldwide and {} thousand cases were diagnosed in 2012.".format(
    key, cancer_rank_dict[key], cancer_cases_dict[key]))
