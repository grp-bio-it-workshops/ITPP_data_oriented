sample_metadata_dict = {}

in_fh = open("Ecoli_metadata.csv", "r")
for line in in_fh:
    if not line.startswith('sample'):
        sample_id = line.split(',')[0]
        sample_metadata_dict[sample_id] = line.strip()

# Test your code here
print(sample_metadata_dict.values())
