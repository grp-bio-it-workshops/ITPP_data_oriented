{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Python Programming"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## U. Nested Data Structures and Modules"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### U.1 Nested Data Structures"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You now know about the most commonly-used Python data types. There are more, each with characteristics suited to a particular task or situation and, if you like, you can make your own as well. You can see the full list of built-in data structures here: https://docs.python.org/3.5/library/stdtypes.html. You should also have an idea of the ways that you can repeat actions on lists and dictionaries with `for` loops, as well as take decisions based on data using `if` statements. This knowledge and these skills can be applied to some quite sophisticated programming. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One other key to writing effective programs is the ability to encapsulate your data and allow them to be accessed and analysed in a way that is efficient and appropriate. If you don't take the time to consider the best way to capture, store, and access your data, you can end up writing programs that are much more complicated and error-prone than they need to be. This can cost you a lot more time in the future. As with any project, a little time spent planning at the beginning can save a lot of time later on."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### _Warmup exercise_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Imagine, you woke up this morning and you had several ideas for things you want to do today, each can be described by a word or a short sentence. You want to write down those ideas (store them in Python), which data type is in your opinion best suited to host all of that data?\n",
    "- an integer\n",
    "- a string\n",
    "- a list\n",
    "- a group\n",
    "- a dictionary\n",
    "- other"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[write down answer]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After compiling your collection of all the things you want to get done today, you realized that they can and should be categorized into different projects like 'work', 'home' etc.\n",
    "Given your response to the question above, how would you store your data into multiple levels?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[write down answer]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Combining Structures"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Often, datasets are best encapsulated by combining the basic data structures together to form larger, well-organised collections that fit the characteristics of the dataset. This combination of multiple data structures is often referred to as 'nesting'. Getting used to dealing with these structures as they grow larger and more complicated can take a bit of time, but it becomes easier with practise and is very important for good programming."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To make this easier to understand, let's begin with looking at [this tabular (comma-separated) data](https://raw.githubusercontent.com/datacarpentry/R-genomics/gh-pages/data/Ecoli_metadata.csv). This file is available in the main GitHub repo as 'Ecoli_metadata.csv'. This example file and related materials are based on the R-Genomics lesson of [Data Carpentry](https://datacarpentry.org/lessons/#genomics-workshop).\n",
    "\n",
    "Please download this file and open to see that its content by extracting its header:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "in_fh = open(\"Ecoli_metadata.csv\", \"r\")\n",
    "print(in_fh.readline())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_Metadata Information_\n",
    "\n",
    "We are studying a population of Escherichia coli (designated Ara-3), which were propagated for more than 40,000 generations in a glucose-limited minimal medium. This medium was supplemented with citrate which E. coli cannot metabolize in the aerobic conditions of the experiment. Sequencing of the populations at regular time points reveals that spontaneous citrate-using mutants (Cit+) appeared at around 31,000 generations. This metadata describes information on the Ara-3 clones and the columns represent:\n",
    "\n",
    "- Column - Description\n",
    "- sample - clone name\n",
    "- generation - generation when sample frozen\n",
    "- clade - based on parsimony-based tree\n",
    "- strain - ancestral strain\n",
    "- cit - citrate-using mutant status\n",
    "- run - Sequence read archive sample ID\n",
    "- genome_size - size in Mbp (made up data for this lesson)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# now read the entire file using a for loop\n",
    "in_fh = open(\"Ecoli_metadata.csv\", \"r\")\n",
    "for line in in_fh:\n",
    "    print(line.strip())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That was a lot of output, but it also means that we have a correct file. Let’s check the top (the first 5 lines) of this dataset using `slicing` and the `readlines()` function:\n",
    "\n",
    "```\n",
    "in_fh.readlines()[:5]\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "in_fh = open(\"Ecoli_metadata.csv\", \"r\")\n",
    "for line in in_fh.readlines()[:5]: \n",
    "# we saw this in the first chapter while accessing a range of items\n",
    "    print(line.strip())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_Exercise 1_**\n",
    "\n",
    "Complete the following code that creates a dictionary that uses the sample name as the key that is linked to the entire line as its value. Make sure that the header is not included in the dictionary. Check it by printing all the keys below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sample_metadata_dict = {}\n",
    "\n",
    "in_fh = open(---)\n",
    "for line in in_fh:\n",
    "    if --- line.---('sample'):\n",
    "        sample_id = line.---(',')[0]\n",
    "        # remove the training '\\n' from each line\n",
    "        sample_metadata_dict[sample_id] = line.---() \n",
    "        \n",
    "# Test your code here\n",
    "print(---)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# %load solution/solution_5_1.py"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Access the values linked to the following keys: ZDB429, ZDB99 and CZB199\n",
    "sample_metadata_dict[\"ZDB429\"]\n",
    "sample_metadata_dict[\"ZDB99\"]\n",
    "sample_metadata_dict[\"CZB199\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you will see that the values for each key is a string, which is comma separated. \n",
    "\n",
    "The values that the keys point to can be any Python value such as an integer, a string, a list or even a dictionary. You will come across values where the keys point to a list of lists, or lists of dictionaries or even dictionaries of dictionaries.\n",
    "\n",
    "You can see this for yourself by converting the current values in the dictionary (strings) to a list using the `split()` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sample_metadata_dict = {}\n",
    "in_fh = open(\"Ecoli_metadata.csv\", \"r\")\n",
    "for line in in_fh:\n",
    "    if not line.startswith('sample'):\n",
    "        sample_id = line.split(',')[0]\n",
    "        sample_metadata_dict[sample_id] = line.strip().split(',')\n",
    "print(sample_metadata_dict.values())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Congratulations, you now have your first nested data structure: a dictionary of lists."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_Exercise 2_**\n",
    "\n",
    "Let's expand the code above.\n",
    "\n",
    "Create a function called `check_genome_size` that takes the `sample_metadata_dict` we created above as input and returns a list of sample names for which the genome size (last item in the value list) is smaller than 4.6 Mbp. \n",
    "\n",
    "Please note that you will have to change the value for genome size to float. If not converted to float, Python will read it as string by default and raise the following error message:\n",
    "\n",
    "```\n",
    "TypeError: '<' not supported between instances of 'str' and 'float'\n",
    "```\n",
    "\n",
    "Test your code by calling the function and printing the returned list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def check_genome_size(value_dict):\n",
    "    smaller_genome_size = []\n",
    "    --- # write your for loop here\n",
    "    return smaller_genome_size\n",
    "\n",
    "sample_metadata_dict = {}\n",
    "in_fh = open(\"Ecoli_metadata.csv\", \"r\")\n",
    "for line in in_fh:\n",
    "    if not line.startswith('sample'):\n",
    "        sample_id = line.split(',')[0]\n",
    "        sample_metadata_dict[sample_id] = line.strip().split(',')\n",
    "# Call you function\n",
    "check_genome_size(sample_metadata_dict)\n",
    "\n",
    "# test your code here"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# %load solution/solution_5_2.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is evident that further data processing can be done by writing different functions.\n",
    "\n",
    "We will however finish with writing another exercise that will give you an idea of how to make or deal with a dictionary of dictionary.\n",
    "\n",
    "**_Example_**\n",
    "\n",
    "1. Create a dictionary that contains two keys: 'plus' and 'minus'\n",
    "    - 'plus' and 'minus' indicate the positive and negative status of citrate-using mutants.\n",
    "2. The values stored in this dictionary will be yet another dictionary which has the sample name as the key, and the sample's properties as the values."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_Part 1_**\n",
    "\n",
    "Let's start by creating two dictionaries `cit_plus_dict` and `cit_minus_dict` that essentially looks like `sample_metadata_dict` (exercise 1), except that `cit_plus_dict`contains only those information that corresponds to the positive cit status and `cit_minus_dict` contains rest of the sample information."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "in_fh = open(\"Ecoli_metadata.csv\", \"r\")\n",
    "\n",
    "cit_plus_dict = {}\n",
    "cit_minus_dict = {}\n",
    "\n",
    "for line in in_fh:\n",
    "    if not line.startswith(\"sample\"):\n",
    "        cit_info = line.split(',')[4]\n",
    "        if cit_info == 'plus':\n",
    "            cit_plus_dict[line.split(',')[0]] = line.strip()\n",
    "        elif cit_info == 'minus':\n",
    "            cit_minus_dict[line.split(',')[0]] = line.strip()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we can put these two dictionaries in another dictionary that has only two keys: plus and minus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "metadata_cit_dict = {}\n",
    "metadata_cit_dict[\"plus\"] = cit_plus_dict\n",
    "metadata_cit_dict[\"minus\"] = cit_minus_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_Part 2_**\n",
    "\n",
    "**Accessing nested data structure.**\n",
    "\n",
    "Using `metadata_cit_dict`, print cit status, sample ids and genome size for each entry using string formatting."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "for cit_status in metadata_cit_dict.keys(): # main keys \"plus\" and \"minus\"\n",
    "    for sample_names in metadata_cit_dict[cit_status]:\n",
    "        sample_values = metadata_cit_dict[cit_status][sample_names]\n",
    "        genome_size = sample_values.split(\",\")[-1]\n",
    "        \n",
    "        # Add a print command here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Modify the above code to print only those entries which have a genome size smaller than 4.6 Mbp."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "for cit_status in metadata_cit_dict.keys(): # main keys \"plus\" and \"minus\"\n",
    "    for sample_names in metadata_cit_dict[cit_status]: # sample names are keys here\n",
    "        sample_values = metadata_cit_dict[cit_status][sample_names]\n",
    "        genome_size = sample_values.split(\",\")[-1]\n",
    "        if ---: # write the condition here\n",
    "            print(\"Sample {} has {} cit status and its genome size is {}\".format(\n",
    "            sample_names, cit_status, genome_size))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can think about writing your codes in functions to make them reusable.\n",
    "\n",
    "If you are interested in exploring more nested data structures, check out [this chapter](https://git.embl.de/grp-bio-it/ITPP/blob/master/3_NestedDataStructures.ipynb) from a different version of this course material on the git repo: https://git.embl.de/grp-bio-it/ITPP."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### U.2 Libraries and Modules\n",
    "\n",
    "One of the great things about Python is the free availability of a huge number of libraries (also called package) that can be imported into your code and (re)used.\n",
    "\n",
    "Modules contain functions for use by other programs and are developed with the aim of solving some particular problem or providing particular, often domain-specific, capabilities. A library is a collection of modules, but the terms are often used interchangeably, especially since many libraries only consist of a single module (so don’t worry if you mix them).\n",
    "\n",
    "In order to import a library, it must available on your system or should be installed.\n",
    "\n",
    "A large number of libraries are already available for import in the standard distribution of Python: this is known as the standard library. If you installed the Anaconda distribution of Python, you have even more libraries already installed - mostly aimed at data science.\n",
    "\n",
    "Importing a library is easy:\n",
    "\n",
    "Import (keyword) + library name, for example:\n",
    "import os    # contains functions for interacting with the operating system\n",
    "import sys   # contains utilities to process command line arguments\n",
    "\n",
    "More at: https://pypi.python.org/pypi"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import  os\n",
    "\n",
    "# Get current directory\n",
    "cwd = os.getcwd()\n",
    "print(cwd)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Make new directory\n",
    "os.mkdir('test_dir')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "help(os)            # manual page created from the module's docstrings"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will not go in detail of this module, but you get more usage from [this notebook](https://github.com/malvikasharan/software-carpentry-embl-2017/blob/master/Day2/3_1-functions-and-modules.ipynb). \n",
    "\n",
    "In the later chapters we will discuss more modules for reading tabular data, analysing and visualizing them.\n",
    "\n",
    "For now, read the \"[The Zen of Python](https://www.python.org/dev/peps/pep-0020/)\" by importing the module called \"this\". \n",
    "> Long time Pythoneer Tim Peters succinctly channels the BDFL's guiding principles for Python's design into 20 aphorisms, only 19 of which have been written down."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import this"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Your Own Modules**\n",
    "\n",
    "Whenever you write some python code and save it as a script, with the .py file extension, you are creating your own module. If you define functions within that module, you can load them into other scripts and sessions.\n",
    "\n",
    "Some Interesting Module Libraries to Investigate:\n",
    "    \n",
    "- os\n",
    "- csv\n",
    "- sys\n",
    "- shutil\n",
    "- urllib2\n",
    "- random\n",
    "- collections\n",
    "- math\n",
    "- argparse\n",
    "- time\n",
    "- datetime\n",
    "- numpy\n",
    "- scipy\n",
    "- matplotlib\n",
    "- pandas\n",
    "- scikit-learn\n",
    "- requests\n",
    "- biopython\n",
    "- openpyxl"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Summary\n",
    " \n",
    "- The elements of data types like lists or dictionaries can themselves be things like lists or tuples or dictionaries, allowing arbitrarily complex data structures to be built up.\n",
    "- `for` loops can be nested in order to access every entry in these complex structures.\n",
    "- Individual entries can be accessed by stacking up indices/keys, which Python interprets one at a time, from left to right.\n",
    "- The values of variables can be inserted into strings, with their format controlled, using the .format() string method.\n",
    "- A module is a piece of software that has a specific functionality. Packages are namespaces which contain multiple packages and modules themselves. See [here](https://docs.python.org/3/tutorial/modules.html) for detail.\n",
    "- Python has several built in modules that we frequently use while programming. Check out the full list of built-in modules in the Python standard library [here](https://docs.python.org/3/library/)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**There is an [xkcd](https://xkcd.com/353/) for everything**\n",
    "\n",
    "<img align=\"left\" src=\"https://imgs.xkcd.com/comics/python.png\">"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
