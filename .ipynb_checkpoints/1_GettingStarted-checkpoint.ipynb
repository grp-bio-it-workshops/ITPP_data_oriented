{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Python Programming"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## 1. Getting Started"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Running Python"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python is one of the preferred programming languages for beginners to learn. Python is commonly used to run programs that are written for specific tasks, like data analysis, visualization, web-scrapping or solving a bioinformatics problem. However, Python also allows you to explore the language itself by typing and executing 'commands'.\n",
    "\n",
    "Python commands or scripts is executed line by line within the Python interpreter. Later in the course we will introduce you to different interpreters, but for now we will use Jupyter Notebook, which is an interactive environment for writing and running code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you install Anaconda, it creates a program group containing a few different items. Included in this an interactive python notebook, 'jupyter notebook', which you should launch using anaconda navigator."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As instructed by your instructors, download the entire folder of course materials and open this first worksheet navigating through the Jupyter Notebook browser.\n",
    "\n",
    "You will view this notebook interactively and you can execute your commands directly within the notebook code cells. We have inserted cells especially for you to play around with the code in. If you use these cells, you can execute the code within them with `CTRL+ENTER` and the output will be printed beneath the cell. Don't worry if you can't use this notebook interactively - you should just type all of the commands in the shell instead.\n",
    "\n",
    "![](http://www.rpgroup.caltech.edu/bige105/code/images/anaconda_navigator.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the window image above, the `In[1]` is the Python prompt and it should have a flashing cursor.  As is traditional under these circumstances, the first thing you should do is get Python to print out the text “Hello”. To do this, type the command below at the prompt and press return (control+return in the notebook)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print('Hello')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "print('Hello World')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Barring the odd typing mistake you have just run your first Python script.  Here, 'print' is a Python function that tells the interpreter that you want it to output the things which follow in the brackets.  The 'Hello World' is a _string_ and is what you want the `print` function to print out."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Playing with Python"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Jupyter Notebook is a great way to experiment with Python.  It’s something that I return to every time I find myself thinking “I wonder what happens if you do this?” or “What’s the best way to do that?”.  You can’t break anything by trying something out and if you do make a mistake, at least you will get an error message that you might be able to decode. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can play around with Python in the notebook's code cells (we have left some blank ones for you to try out your own commands in) and execute the cell once you are done typing. The output of your commands will appear underneath. If you have made a mistake, or you want to try something else, you can edit the code in the cell and execute it again when you're done."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For longer, multi-line programs you will probably find it easier to use a text editor. A second item, 'spyder', is a powerful text editor with the capacity to run your Python scripts in a dedicated shell window. You should launch this and use it when you start writing multi-line code that you will want to go back and edit/append as you go through the course. We will talk about it later during this course."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Literals\n",
    "\n",
    "Values of a _type_, presented literally."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# example        name       type designation\n",
    "42             # integer    int\n",
    "2.016          # float      float*\n",
    "\"Homo sapiens\" # string     str\n",
    "\"42\"           # string     str (can be defined by using \"\"/'')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- int: whole numbers e.g. 1, 1000, 6000000000\n",
    "- float: 'floating point' non-whole numbers e.g. 1.9, 30.01, 10e3, 1e-3\n",
    "- string: ordered sequence of characters, enclosed in quotation marks (single, double, _triple_)\n",
    "    \n",
    "#### Comments (#)\n",
    "\n",
    "Comments are preceded by a #, and are completely ignored by the python interpreter. Comments can be on their own line or after a line of code.\n",
    "\n",
    "Comments are an incredibly useful way to keep track of what you are doing in your code. Use comments to document what you do as much as possible, it will pay off in the long run."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# what is the type of a value?\n",
    "print(type(42))\n",
    "\n",
    "# type conversions\n",
    "print(float(42))\n",
    "print(type(float(42)))\n",
    "\n",
    "print(str(42))\n",
    "print(type(\"42\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Evaluating Expressions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Python shell can also be used to evaluate expressions, allowing you either to perform calculations interactively, or more usually to check more complicated expressions interactively before putting them in your programs.  The Python shell allows you to do all of the normal operations, in pretty much the way you would expect."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Have a go with some expressions, such as: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "3 * 4     # Multiplication"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# type your command(s) here or use the IPython shell..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "7 + 10    # Addition"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# type your command(s) here or use the IPython shell..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "7 - 10    # Subtraction "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# type your command(s) here or use the IPython shell..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "10 / 3    # Division"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# type your command(s) here or use the IPython shell..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So far, so good, but don’t limit yourself to the examples here.  Try some of your own and make sure you understand the results.  There are a few other operators, though, which you might not be as familiar with.  Try these:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "10 % 3     # Modulus (remainder)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# type your command(s) here or use the IPython shell..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "2 ** 10    # Exponentiation (2 to the power of 10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# type your command(s) here or use the IPython shell..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, as expected.  However, there are a few things that you need to be aware of when using arithmetic in any programming language.  In Python v2.x, if your numbers are integers, Python with return an _integer_ value.  So try: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "10 / 7"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# type your command(s) here or use the IPython shell..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here you should get the answer 1.4285714285714286 if you're using Python v3.x. This has been changed in Python 3.x., and 10/7 now gives the result you most likely expect. Of course, you might actually only want the integer result in the first place, and regardless of the version you can force Python to give you that as well using so-called “floor division”:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "10.0 // 7"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# type your command(s) here or use the IPython shell..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python can handle some very large numbers. For example, it can easily deal with raising 2 to the power of 32:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "2 ** 32 "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python can deal with numbers slightly larger than this too, so"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "2 ** 64"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and even"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "2 ** 1024"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "work just fine. You can go even higher, so raising to the power of 100,000"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "2 ** 100000"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "is quite OK. Though I must admit that I haven’t actually checked that the 30103 digits of this result are correct."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some of these operators don’t just work on numbers, `+` and `*` can be used on strings as well.  Strings are just sequences of characters enclosed in quotation marks like the 'Hello, World' above.  Python doesn’t mind if you use single or double quotes as long as you don’t mix them.  \"Addition\", `+`, concatenates two (or more) strings together to return a new longer string.  \"Multiplication\", actually repetition, `*`, takes a number and a string and repeats the string that many times in a new string: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "'Hello, ' + \"world!\" "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# type your command(s) here or use the IPython shell..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "'Hello ' * 8 "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# type your command(s) here or use the IPython shell..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "9 * 'Hello...'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# type your command(s) here or use the IPython shell..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### _Exercise 1.1_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Try to use expressions that you would use in your normal work and see if they give the results you expect.  Explore using brackets to group sub-expressions (things in brackets are always evaluated before everything else).  Before you move on to the next section, which of the following expressions would correctly calculate the hypotenuse of a right-angled triangle, with sides length 12 and 5?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "a)\n",
    "```Python\n",
    "(12*2 + 5*2)/2\n",
    "```\n",
    "b)\n",
    "```Python\n",
    "(12**2 + 5**2)**0.5\n",
    "```\n",
    "c)\n",
    "```Python\n",
    "(12^2 + 5^2)^0.5\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# type your command(s) here or use the IPython shell..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Using Variables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So far, we have just been playing with what Python calls values.  When you are writing programs, it’s useful to be able to give names to the values that we are dealing with so that once we do a calculation or string manipulation we can refer to the results later.  We do this with an assignment statement, which looks like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "x = 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You’ll notice that, when this line is executed, Python doesn’t return anything. This is also true if you capture the result of one of the expressions that we tried above:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "y = 10.0 / 7"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To look at the values, just type the names of the variables that you have created:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "More normally, you would probably output the results using the `print` statement we started with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print(x, y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the brackets are included in the output above, then you're working in version 2. If not, then you have a version 3 environment. (Whichever version you have, you will be able to work through the rest of this course.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As well as being on the left of an assignment operation, variable names can be used in the expressions as well, so"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "x = x + y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "replaces the value currently referred to by `x` with the new value obtained from adding the values of `x` and `y`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Variable naming\n",
    "\n",
    "Rules:\n",
    "\n",
    "- identifier lookup is case-sensitive\n",
    "  - `myname` & `MyName` are different\n",
    "- must be unique in your working environment\n",
    "  - existing variable will be __over-written without warning__\n",
    "- cannot start with a number, or any special symbol (e.g. $, %, @, -, etc...) except for \"_\" (underscore), which is OK.\n",
    "- cannot have any spaces or special characters (except for \"-\" (hyphen) and \"_\" (underscore))\n",
    "\n",
    "Conventions/good practice:\n",
    "\n",
    "- identifiers (usually) begin with a lowercase letter\n",
    "- followed by letters, numbers, underscores\n",
    "- use a strategy to make reading easier\n",
    "  - `myName`\n",
    "  - `exciting_variable`\n",
    "- long, descriptive > short, vague"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Variables that refer to numbers are fine and are incredibly useful, but they are also one of the less interesting types of Python data.  This is because they only have a value.  Some variables are much more interesting, and string variables are a good example.  To assign a string to a variable you follow the same basic syntax as before:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "my_string = 'The quick brown fox jumps over the lazy dog'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print(my_string)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, you can just type the variable name or use it in a `print` statement, but what makes a variable containing a string more interesting is that it is one of Python’s object data types.  This means that it doesn’t just passively hold the value of the string, it also knows about things you can do to the string.  For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "my_string.upper()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `.` here indicates that the method is part of the string `my_string`, and the brackets indicate that we want to execute it. Here the brackets are empty, but later we will be using them to pass information into the methods.  There are many things that strings can do with themselves, and if you look at the Python cheat sheet, you will see what they all are. Try using them on the string, for example: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "my_string.capitalize()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "my_string.title()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you look at `my_string` itself after any of these, you’ll see it hasn’t changed, these object methods simply return a new version of the string with the appropriate transformation done to it which you can then store in another variable (or back in `my_string`) if you want to.  This is because a string cannot be changed in place (in technical terms, it is _immutable_), only explicitly overwritten with a new value. So, to save the new version of the string, you can type the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "my_string = my_string.title()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can use almost any combination of letters and numbers in the variable names, but you can’t start a variable name with a number. You can’t include spaces (a space is one of the ways that Python can tell that the name is finished) but you can include underscore characters.  Variable names can also begin with underscore, but these tend to be used under special circumstance which you will discover once you start learning about object-oriented programming."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### _Exercise 1.2_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use the .count() method on the string “The quick brown fox jumps over the lazy dog” to count the occurrences of the word “the”."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "my_string = \"The quick brown fox jumps over the lazy dog\" \n",
    "# type your command(s) here or use the IPython shell..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If this returns 1, how could you persuade it that it should be 2?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# type your command(s) here or use the IPython shell..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once you have that sorted out, try"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`my_string.split()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# type your command(s) here or use the IPython shell..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and see if you can understand what it has done."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Lists "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The last exercise returned a result which you might think looks unusual.  My interpreter gave me:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "my_string = \"The quick brown fox jumps over the lazy dog\"\n",
    "my_string.split()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This result is in the form of a _list_ object.  A list is exactly what you might expect based on the name.  It’s a set of values (which can be numbers, strings, objects or even lists, but that is a bit advanced for now) that are kept in a specific order.  You can create a new list using the same format as the result above:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "gene_list = ['TP53', 'TNF', 'EGFR', 'VEGFA', 'APOE', 'IL6']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img align=\"left\" width=\"500\" height=\"700\" src=\"images/nature_genes.jpg\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_Reference: Elie Dolgin, Nov 2017, Nature_ (https://www.nature.com/articles/d41586-017-07291-9)\n",
    "\n",
    "Like strings, lists are a type of object, and so they also have some methods associated with them, which you can use.  However, unlike strings, these methods mostly change the list in place, rather than returning a new list.  So, when you type"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "gene_list.sort()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python doesn’t return a value, but if you look at the list, you will see that the order of the items has changed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "gene_list"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This means that unlike strings, lists are _mutable_, and individual items and sets of items can be changed in place.  If you decide you want to add items to the list, you can do it with the `.append()` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "gene_list.append('TGFB1')\n",
    "gene_list"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you feel it’s important enough to go at the top of the list, you can use `.insert()` to insert the new item at a particular point and move everything else down:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "gene_list.insert(0, 'BRCA1') # The most commonly studied gene in cancer\n",
    "gene_list"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Removing items from the list is just as easy - you use `.pop()` to do that.  If you don’t give it an index, it will remove the last item in the list, otherwise `pop` removes the item with the index you specify and shuffles everything else up one position to close the gap. Give it a try, to remove one of the items in the gene_list. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# type your command(s) here or use the IPython shell..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Sequences"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The two object types that we have talked about so far share a number of properties.  Both strings and lists consist of ordered pieces of data.  In the case of strings this is simple characters.  In lists, the elements of the list can be of any object type."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For both lists and strings we might want to refer to a particular item or range of items in a string or list and we can do this easily:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "words = my_string.split()\n",
    "print(words[3]) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You will see that \"fox\" is actually the fourth word and this is just one of the things that computers do that you have to get used to.  The first element in a sequence has an index of 0, so"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print(words[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "gives you the first item in the list. This is referred to as zero-based indexing or offset numbering.  Its origins are in programming languages where variables actually refer to the memory location of the start of the list, and now has just become a tradition.  Negative indices are assumed to be relative to the end of the array, so"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "words[-1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "yields the final element in the list.  Of course, if we knew how long the sequence was, we could just use the number of the last element.  For any sequence data type, `len()` will tell us how many elements it has:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "len(words)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "len(my_string)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So, since the sequences are indexed from zero, the last element, i.e. `words[-1]`, is the same as `words[len(words)-1]`. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Accessing a range of items**\n",
    "\n",
    "It is also possible to access a range of items from the list by defining the start index and stop index, separated by a colon symbol \":\".\n",
    "\n",
    "For example, to access the item 3-5 from the list `gene_list`, we will use the following syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "gene_list = ['TP53', 'TNF', 'EGFR', 'VEGFA', 'APOE', 'IL6']\n",
    "gene_list[2:5]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Please note that such ranges in python are defined as \"left inclusive and right exclusive\" meaning that the right number is excluded while accessing the items. Which in this case the 6th item (index 5).\n",
    "\n",
    "When accessing the item from the beginning to a stop index, for example, index range 0-4, the start index can be ignored:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "gene_list[:5]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly when accessing the item from the a start index till the end, for example, index range 2-5, the last index can be ignored:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "gene_list[2:]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### _Exercise 1.3_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are a few quirks to sequence indexing (apart from starting at 0), and I have tried to summarise these on the Python Cheat Sheet.  Have a go with a few of the \"Indices and Slices\" and make sure you understand how they work.  Then, instead of trying them on a list, try them on a string.  Now, if you are really adventurous, try to print the third letter of the fourth word in the `words` list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# type your command(s) here or use the IPython shell..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### _Exercise 1.4_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below is a set of commands working with a list of common cancers. First, the list is extended by adding 'Liver' onto the end, and then 'Prostate' - the fourth element in the list - is assigned to the variable `fourth_common_cancer`. Some of the code has been removed (replaced with `---`). Fill in the blanks in the code block to make it work."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Examples of most common cancers worldwide \n",
    "# Ref: https://www.wcrf.org/int/cancer-facts-figures/worldwide-data\n",
    "\n",
    "common_cancers = ['Lung', 'Breast', 'Colorectum', 'Prostate', 'Stomach']\n",
    "# add 'Liver' onto the end of the list \n",
    "common_cancers.---('Liver') \n",
    "# access the fourth entry in the list\n",
    "fourth_common_cancer = common_cancers[---] "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Summary"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Python can be used to calculate the results of a wide range of expressions, and print out the results.\n",
    "* Like in all programming languages, variables can be used to store the results of calculations or simple values that we might need to use later.\n",
    "* Variables refer to values.  The values can be numbers (integer and floating point), strings or lists.  You will discover soon that there are other data types as well.\n",
    "* Some data types are objects, which have methods associated with them.  These methods perform common tasks on the values of the objects.\n",
    "* Some data types are sequences, which let us access individual elements at will.\n",
    "* Sequence data types allow us to step through the values in them, but to do that we need to take some first steps towards writing real programs."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### _Debugging Exercise_\n",
    "\n",
    "The lines below have mistakes in them, which produce errors when they are run. Try to identify the problems in the code, and fix them so that they run without error."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "coffee_break = ['coffee', 'tea'; 'cookies', 'fruits']\n",
    "coffee_break.append['water']\n",
    "  print(coffee_break)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
