{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Python Programming"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. File Handling"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Opening Files\n",
    "\n",
    "What we have been doing so far has required you to type the data into the programs by hand, which is a bit cruel. For this worksheet, we will be using a larger dataset (still tiny by many standards) and you can download a file containing the data from the main GitHub repository where these teaching materials are maintained. Just save the linked file into the same directory as you are keeping the Python scripts. (_Note: if you already obtained these materials from the repository, you probably downloaded the data file, into the same folder, at the same time._)\n",
    "\n",
    "Of course, this requires that we know how to get data out of the file and into our Python program and that is what we are going to do in this worksheet. Specifically we are talking about reading data out of text files. Binary files face their own challenges, and I am not going to get into that in this course since handling them is very dependent on the implementation of the binary file. In any case, for a number of significant classes of binary files, such as images, BAM files or NetCDF formatted data, there are already Python modules to enable you to access the data in a simple way. But in any case, we will look at text files for now and firstly we need to know how to open them.\n",
    "\n",
    "If you have downloaded the file, you should make sure that it is saved into the same folder where you are going to save the python programs that you will use to analyse it. We will start simple, just by opening the file at the Python shell prompt."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### _Exercise 3.1_\n",
    "\n",
    "**File 1: patients.txt**\n",
    "\n",
    "The file consists of the patient ids"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "PatientId\n",
    "P1\n",
    "P2\n",
    "P3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first thing we're going to do is open the file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "file_handle = open('patients.txt', 'r')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The file is now open, and `file_handle` is a variable referring to a _file_ data type.  As you might have guessed, the first argument for `open` is a string containing the filename, but the `'r'` probably needs to be explained. This argument is called the file mode, and `'r'` means that you only want to read data. \n",
    "\n",
    "If you specify `'w'`, it means that you want to write data into the file, which we will talk about later. One very important point is that when you open a file that already exists for writing, the contents of the file are cleared, and can’t be recovered. \n",
    "\n",
    "If you instead want to append data to an existing file you should specify `'a'` as the mode."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you might expect by now, file objects have their own methods and you can use some of these to read data from the file.  The easiest way of doing this is to use `.readlines()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "line = f.readlines() #reads the entire file and returns entry list\n",
    "line"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The variable `lines` now refers to a list of strings containing each of the lines in the file. Try looking at one or two of them. If you didn’t look at the contents of the file before you opened it with your program, have a look at it now. If you compare `lines[1]` in Python with the second line in the file, you will see some differences. Most obvious is the presence of a `\\n` at the end of each line in the Python list. These are _newline characters_ and we need to remember to remove these when we process the data from the file. Although it looks like two characters, it is what is called an escape character: just a single character but one with special meaning to the program and which we cannot normally see in a string."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Getting Data from Files\n",
    "\n",
    "Using `.readlines()` to create a list containing all of the lines is nice and simple, but has a major drawback. It’s fine when your file is small enough to read all of the lines into memory, but if you are reading a 32Gb SAM file, you are likely to run into problems. Here, you want to read one line at a time, and process it. Python files do have a `.readline()` method that will read only one line, but it’s best to just use a `for` loop. Python has an idea of 'iterable' data types which you can put into `for` loops. We have seen two of these so far: the list and the dictionary. For a list you get each element in turn, and for a dictionary you get each key in turn. Strings are also iterable and return each character in turn. The point of mentioning this now is that files are also iterable, and Python tries to pass you exactly what we want: one line at a time. So we can start to write a program now to start processing this data file. \n",
    "\n",
    "__Note__ This will be the largest program you have written so far, and what I do when I am embarking on writing a large program is to start with just the basic structure and make sure that works then add to the program step by step and keep running it to make sure it is doing what I expect before it gets too complicated.\n",
    "\n",
    "To begin, __in an editor window__, type"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "datafile = open('patients.txt', 'r')\n",
    "for line in datafile:\n",
    "    print(line)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "OK, so far so good, the program is basically printing the whole file out to the Python Shell window. However, I forgot about the newline characters at the end of the lines. You have probably noticed that the `print` statement automatically adds a newline to the end of everything it prints, so now we are getting two after each line, which is why the output is double-spaced. So the first thing to do is to fix that, by removing the newline characters from the lines as we read them in. Strings have a `.strip()` method which removes any newlines, spaces or tabs (we call these characters 'whitespace') at the start and end of each line. So add the line"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "line = line.strip()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "to the loop before the print statement (at the correct level of indentation) and try the program again.  Now the output should look single spaced."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "datafile = open('patients.txt', 'r')\n",
    "for line in datafile:\n",
    "    line = line.strip()\n",
    "    print(line)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating lists using data from the file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### _Exercise 3.2_\n",
    "\n",
    "How can we make a list that contains all the lines from the file?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "patient_data = []\n",
    "\n",
    "datafile = open('patients.txt', 'r')\n",
    "for line in datafile:\n",
    "    patient_data.append(line.strip())\n",
    "print(patient_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is very similar to `datafile.readlines()`, however here we read the file line by line an not the entire file together."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Optional Exercise**\n",
    "\n",
    "Run a `for` loop over one of the above lists and using string formatting print corresponding items from all the three lists."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### _Example_\n",
    "\n",
    "There are two other files called 'patient_height.txt' that contains heights of the patients in the same order as the 'patient.txt' and 'patient_weight.txt' contains patients' weight. \n",
    "\n",
    "**Task**: Create lists containing heights and weights of the patients."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# example: list of patient ids\n",
    "\n",
    "datafile = open(filename, 'r')\n",
    "\n",
    "list_name = []\n",
    "\n",
    "for ...:\n",
    "    list_name.append(...)\n",
    "list_name"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_Limitations of this approach:_**\n",
    "\n",
    "1. The code reads the entire file and uses up the computer memory, which is again not efficient when dealing with bigger file size.\n",
    "2. You need to know the content of the file which can get challenging as the file size increase.\n",
    "3. This file format can introduce errors if the different data points are not connected"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_Solution_**\n",
    "\n",
    "**Create better input files**\n",
    "\n",
    "Tabular files (e.g. character/items separated by comma, tab, colon etc.) are a very common (and practical) way to store data. Every single row corresponds to a sample, and each column coresponds to a measurement or variable. The file 'patients_table.csv' contains the same data we have been working with formatted as a `.csv` file (which stands for \"comma separated values\")\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "datafile = open('patients_table.csv', 'r')\n",
    "\n",
    "for lines in datafile:\n",
    "    print(lines.strip())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### `split` for splitting"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# test example\n",
    "\n",
    "test_string = \"this is a test string\"\n",
    "test_string.split() "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python `split` function can be used for breaking a large string down into smaller chunks, or strings, using a defined separator. A separator  (or delimiter) is any character (i.e. space: ' ', comma: `,`, semicolon: ';', newline: '\\n', tab: '\\t' etc) or a letter/word that placed between the variables.\n",
    "\n",
    "As shown in the example above, if no separator is defined, whitespace will be used by default."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "test_string = \"this,is,a,test,string\"\n",
    "test_string.split(',') # splitting by comma and generating a list\n",
    "test_string.split(',')[3] # splitting by comma and accessing the 4th item"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_Exercise 3.4_**\n",
    "\n",
    "Using `split` for the file 'patients_table.csv'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "datafile = open('patients_table.csv', 'r')\n",
    "\n",
    "# create a patient id list\n",
    "patient_id = []\n",
    "for lines in datafile: # using headers as separaters\n",
    "    pid = lines.split(',')[0]\n",
    "    patient_id.append(pid)\n",
    "print(patient_id)\n",
    "\n",
    "# extend the code above by removing the 'header' 'PatientId' from the list\n",
    "    \n",
    "# create other lists below"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_Exercise 3.5: Part-1_**\n",
    "\n",
    "Read the file 'speciesDistribution.txt' and print all the lines of the file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "in_file = open('speciesDistribution.txt', 'r')\n",
    "\n",
    "--- lines --- in_file:\n",
    "    print(---)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This file consists of two types of data. Some lines contain the names of sampling sites and some contain a letter and a number. The letters are taxon designators and the numbers represent abundance of that taxon at that particular site (in this case, as measured by high-throughput DNA sequencing of 18S rRNA). We need to process the two line types differently and store the information in a suitable data structure.\n",
    "\n",
    "Take a moment to think about how you think we might go about doing that, and what the best data structure type to use might be for storing the taxon codes and counts for each site. Don’t worry if you find this a little confusing and/or daunting: we are going to work through it one step at a time, starting by identifying each site described in the data.\n",
    "\n",
    "The lines with the site names in them all start with the substring `Site:`, so they are easy to recognise.  We can use the string’s `.startswith()` method in an if statement to identify these lines so that we can process them separately.  Try using this method at the Python command line so you understand how it works before putting it into the program."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_Exercise 3.5: Part-2_**\n",
    "\n",
    "Change the program to only print out the lines that start with `Site:`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "--- lines --- in_file:\n",
    "    if lines.startswith(---)\n",
    "        print(---)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating file\n",
    "\n",
    "Like reading files, Python allows you to create (write), overwrite and delete files.\n",
    "\n",
    "To create a file we use similar `open()` command, but instead of `r` (reading), we use `w` (writing)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Example:\n",
    "\n",
    "out_fh = open(\"species_site_names.txt\", \"w\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If file does not exist, it creates a new file. If file exists it overwrites the file. \n",
    "\n",
    "We can write lines to the file using the `write` method:\n",
    "\n",
    "```\n",
    "out_fh.write(\"Add this line\")\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After writing to a file, we need to make sure to close the file. This ensures that any remaining output is written to the file:\n",
    "\n",
    "```\n",
    "out_fh.close()\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_Exercise 3.5: Part-3_**\n",
    "\n",
    "From the previous code (exercise 2.2.5) remove the `Site:` substring (and the space that follows it) from the string to print the actual site name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_Exercise 3.6: Part-4_**\n",
    "\n",
    "Now write the site names in the file 'species_site_names.txt'. Make sure that a new line (\"\\n\") is included with every entry."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_We will create more files in the next chapter!_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Reading multiple files\n",
    "\n",
    "For this exercise, we will use the [worldwide cancer data](https://www.wcrf.org/int/cancer-facts-figures/worldwide-data) that we saw in the first chapter.\n",
    "\n",
    "In the main GitHub repository we have provided three *.tsv* (tab ('\\t') separated values) files: 'worldwide_cancer.tsv', 'worldwide_male_cancer.tsv' and \"worldwide_female_cancer.tsv'."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_Exercise 3.6: Part 1_**\n",
    "\n",
    "1. Read all the files\n",
    "2. Create three empty lists to store cancer names from different files\n",
    "3. Use `for` loop to go through the files and extract cancer names\n",
    "4. Store cancer names in the lists created in the point 2.\n",
    "5. Print all the lists."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_Exercise 3.6: Part 2_**\n",
    "\n",
    "6. Read the male and female cancer lists, and print the entries common to both."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Bonus: `set` for `union` and `intersection`**\n",
    "\n",
    "In previous chapter we saw `+` for concatanating two lists and set for creating set of unique items. Sets allow us to use mathematical operations like `union` and `intersection`.\n",
    "\n",
    "Example of `union`:\n",
    "\n",
    "```\n",
    "list(set(male_cancer_list).union(set(female_cancer_list)))\n",
    "```\n",
    "\n",
    "**_Exercise 3.6: Part 3 (optional)_**\n",
    "\n",
    "7. Use `intersection` to print common entries from male and female cancer lists"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Code here"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# %load solution/cancer_file_handling.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Summary"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. Python provides an inbuilt function for file handling by writing (\"w\") and reading (\"r\") files.\n",
    "2. `strip()` is used to remove any all chars (stripping) from the beginning and the end of the string.\n",
    "3. Biological data is stored in files of different file formal (.txt, .csv, .tsv, .tab, .fasta, .gff etc.). To make these files computer readable, variables are stored by separating them by characters or symbols.\n",
    "5. `split()` is used for extracting different variable separated by a certain character. It is very useful when reading a tabular data. \n",
    "6. Multiple files can be imported in a python script to store their data in different dataframes (like lists and sets) and analysed to gain information. "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
